# FilSorek

That is a repo for embedded hardware for FilSorek - The sensor for Filka.io.
This repo is using ArduinoIDE, with ESP32-C3 MCU, as well as SuplaDevice libs from Supla. All of these you can install via ArduinoIDE.


The idea behind this project is to create simple modular sensor, easy to build and 3d printed by anyone.
It should have modules like:
- soil moisture sensor
- temp&humidity sensor
- light sensor module
- cam module to detect the health of the plant


  PCB should be as simple as possible, to be easy prepareable using simple technologies