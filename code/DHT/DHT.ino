/*
Copyright (C) AC SOFTWARE SP. Z O.O.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

https://www.waveshare.com/w/upload/0/0b/Esp-c3-32s-kit-v1.0_specification.pdf

*/

#include <SuplaDevice.h>
#include <supla/sensor/DHT.h>

// Choose proper network interface for your card:
#ifdef ARDUINO_ARCH_AVR
  // Arduino Mega with EthernetShield W5100:
  #include <supla/network/ethernet_shield.h>
  // Ethernet MAC address
  uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05};
  Supla::EthernetShield ethernet(mac);

  // Arduino Mega with ENC28J60:
  // #include <supla/network/ENC28J60.h>
  // Supla::ENC28J60 ethernet(mac);
#elif defined(ARDUINO_ARCH_ESP8266) || defined(ARDUINO_ARCH_ESP32)
  // ESP8266 and ESP32 based board:
  #include <supla/network/esp_wifi.h>
  Supla::ESPWifi wifi("WIFI SSID HERE", "WIFI PASSPHRASE HERE");
#endif

/*
 * This example requires DHT sensor library installed. 
 * https://github.com/adafruit/DHT-sensor-library
 */

#define DHT1PIN 8
#define DHT1TYPE DHT22
//#define DHT2PIN 25
//#define DHT2TYPE DHT22
 
void setup() {

  Serial.begin(115200);

  // Replace the falowing GUID with value that you can retrieve from https://www.supla.org/arduino/get-guid
  char GUID[SUPLA_GUID_SIZE] = {0xFE,0xAF,0x59,0x18,0x07,0xFF,0x43,0x78,0xD7,0x85,0xAA,0xC1,0x5E,0x3D,0x0A,0xD5};

  // Replace the following AUTHKEY with value that you can retrieve from: https://www.supla.org/arduino/get-authkey
  char AUTHKEY[SUPLA_AUTHKEY_SIZE] = {0xDC,0xDE,0xA1,0xF1,0x92,0x89,0xE4,0x76,0xE8,0x99,0xB0,0xCC,0xB2,0xDD,0x42,0xC2};

  /*
   * Having your device already registered at cloud.supla.org,
   * you want to change CHANNEL sequence or remove any of them,
   * then you must also remove the device itself from cloud.supla.org.
   * Otherwise you will get "Channel conflict!" error.
   */
    
  // This example adds two DHT22 sensors. 
  
  // CHANNEL0 - DHT22 Sensor
  new Supla::Sensor::DHT(DHT1PIN, DHT1TYPE);

  // CHANNEL1 - DHT22 Sensor
//  new Supla::Sensor::DHT(DHT2PIN, DHT2TYPE);

  /*
   * SuplaDevice Initialization.
   * Server address is available at https://cloud.supla.org 
   * If you do not have an account, you can create it at https://cloud.supla.org/account/create
   * SUPLA and SUPLA CLOUD are free of charge
   * 
   */

  SuplaDevice.begin(GUID,              // Global Unique Identifier 
                    "svr64.supla.org",  // SUPLA server address
                    "YOUR MAIL HERE",   // Email address used to login to Supla Cloud
                    AUTHKEY);          // Authorization key
    
}

void loop() {
  SuplaDevice.iterate();
}
